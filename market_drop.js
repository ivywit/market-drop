const { hostname } = window.location;

// Asana
if (hostname.match('asana.com$')) {
  document.querySelector('.UpgradeButton').remove();
  document.querySelector('.SidebarTopNavLinks-myPortfoliosbutton').remove();
  document.querySelector('.SidebarTopNavLinks-goalsButton').remove(); 
  document.querySelector('.SidebarReports-upgradeLink').remove();
}
